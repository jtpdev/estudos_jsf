package br.teste.mb;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;

//@RequestScoped // existe apenas em caso de request e é apagado a uma nova requisição
//@ViewScoped // existe apenas quando a página estiver sendo exisbida, quando mudar de página ou recarregar será apagado
//@SessionScoped // existe durante a sessão do usuário, mesmo que ele saia da página, compartilha apenas uma instância do MB com cada usuário.
@ApplicationScoped // existe enquanto a aplicação estiver sendo carregada, compartilhando a mesma instância do MB com todos os usuário.
@ManagedBean
public class NomesBean {
	private String nome;
	private List<String> nomes = new ArrayList<>();

	private HtmlInputText inputNome;
	public HtmlInputText getInputNome() {
		return inputNome;
	}

	public HtmlCommandButton getBotaoAdicionar() {
		return botaoAdicionar;
	}

	public void setNomes(List<String> nomes) {
		this.nomes = nomes;
	}

	public void setInputNome(HtmlInputText inputNome) {
		this.inputNome = inputNome;
	}

	public void setBotaoAdicionar(HtmlCommandButton botaoAdicionar) {
		this.botaoAdicionar = botaoAdicionar;
	}

	private HtmlCommandButton botaoAdicionar;

	public void adicionar() {
		this.nomes.add(nome);
		// desativa campo e botão quando mais que 3 nomes
		// forem adicionados
		if (this.nomes.size() > 3) {
			this.inputNome.setDisabled(true);
			this.botaoAdicionar.setDisabled(true);
			this.botaoAdicionar.setValue("Muitos nomes adicionados...");
		}
	}
	
	public String chameOla() {
		if(nomes.isEmpty())
			return null;
		return "Ola"; 

	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<String> getNomes() {
		return nomes;
	}
}