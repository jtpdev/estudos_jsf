package br.teste.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.teste.pojo.Lancamento;

public class Lancamentos {
	
	private EntityManager manager;

	public Lancamentos(EntityManager manager) {
		this.manager = manager;
	}
	
	public void adicionar(Lancamento lancamento) {
		this.manager.persist(lancamento);
	}

	public List<Lancamento> todos() {
		TypedQuery<Lancamento> query = manager.createQuery("from Lancamento",
				Lancamento.class);
		
//		Ou assim que não se trabalha com JPQL
//		Session session = manager.unwrap(Session.class);
//		Criteria criteria = session.createCriteria(Lancamento.class);
//		return criteria.list();
		return query.getResultList();
	}
}