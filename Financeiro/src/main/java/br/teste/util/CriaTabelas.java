package br.teste.util;

import javax.persistence.Persistence;

/**
 * Continuar de: Introdução ao JSF, capt: 3
 * 
 * @author Jimmy Tinelli Porto
 *
 */
public class CriaTabelas {
	
	public static void main(String[] args) {
		Persistence.createEntityManagerFactory("FinanceiroPU");
	}

}
